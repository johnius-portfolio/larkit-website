$(document).ready ->

  # *** Expand contacts (footer)
  # Viewport variables
  viewport = {}
  viewport.height = $(window).innerHeight()

  # Contacts variables
  contacts = {}
  contacts.id = '#contacts'
  contacts.height = $(contacts.id).outerHeight()
  contacts.bg = {}
  contacts.bg.color = $(contacts.id).css('background-color')
  contacts.bg.newcolor = 'rgb(99,99,99)'
  contacts.animation = {}
  contacts.animation.bg = {}
  contacts.animation.bg.color = {}
  contacts.animation.bg.color.duration = 120
  contacts.animation.bg.color.delay = contacts.animation.bg.color.duration
  contacts.show = ->
    # *** Start [contacts.show]
    # Get the viewport's height
    viewport.height = $(window).innerHeight()

    # Set contacts to fullscreen or leave as is
    # if it's height bigger than viewport
    if viewport.height >= contacts.height
      contacts.newheight = viewport.height
    else
      contacts.newheight = 'auto'

    # Position of contacts from top of page
    contacts.top = $(contacts.id).offset().top


    # Blink contacts background
    $(contacts.id)
    .css('background-color', contacts.bg.color)
    .animate
      backgroundColor: contacts.bg.newcolor
      contacts.animation.bg.color.duration
    .animate
      backgroundColor: contacts.bg.color
      contacts.animation.bg.color.duration

    # Scroll to contacts
    # $('html, body').animate
    #   # Fix for missing pixel in Firefox and IE
    #   scrollTop: contacts.top + 1
    #   contacts.animation.scroll.duration

    # If contacts height is unchanged leave as is
    if contacts.newheight != 'auto'
      $(contacts.id).css('height', parseInt(contacts.newheight))
    # End [contacts.show] ***


  # Check if hash on load is "contacts"
  if window.location.hash == contacts.id
    contacts.show()


  # Bind expanding action on click event
  $('a').on 'click', (e) ->
    if $(this).attr('href') == contacts.id
      # e.preventDefault()
      e.stopPropagation()
      contacts.show()
      window.location.hash = contacts.id
      if !$('#navigation').hasClass 'collapse'
        $('#navigation').collapse 'hide'


  # Disable links with data-action
  $('a').on "click", (e) ->
    if $(this).data('action') == ""
      e.preventDefault()

  # window.footerPosX = $('footer').offset().top

  # $(window).on 'resize', ->
  #   window.footerPosX = $('footer').offset().top

  # $(document).on 'scroll', (e) ->
  #   scrollTop = $(this).scrollTop()
  #   if scrollTop >= footerPosX
  #     console.log scrollTop + " >= " + footerPosX



  # Icon change mechanism (optional)
  # Only for decide what icon to choose
  # icons = ['icon-info', 'icon-bookmark', 'icon-flag', 'icon-alert', 'icon-info-outline', 'icon-info-1']

  # window.keycount = 2
  # element = $('.announcement__icon')
  # $(element).addClass icons[keycount]

  # $(window).on "keydown", (e) ->
  #   key = e.keyCode

  #   switch key
  #     when 37
  #       this.keycount--
  #     when 39
  #       this.keycount++

  #   if keycount < 0
  #     this.keycount = icons.length - 1

  #   if keycount >= icons.length
  #     this.keycount = 0

  #   $(element).removeClass icons.join ' '
  #   $(element).addClass icons[keycount]